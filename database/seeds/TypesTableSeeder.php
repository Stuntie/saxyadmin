<?php

use Illuminate\Database\Seeder;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Type::create([
            'type' => 'Alto'
        ]);
        
        App\Type::create([
            'type' => 'Tenor'
        ]);
        
        App\Type::create([
            'type' => 'Soprano'
        ]);
        
        App\Type::create([
            'type' => 'Baritone'
        ]);
        
    }
}
