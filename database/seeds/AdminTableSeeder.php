<?php

use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Admin::create([
            'role' => 'Admin',
            'description' => 'Admin. Full access'
        ]);

        App\Admin::create([
            'role' => 'Staff',
            'description' => 'Shop Staff. Access to Orders'
        ]);

        App\Admin::create([
            'role' => 'Customer',
            'description' => 'Customer. No access outside of Web Store. Access to own profile and Orders only.'
        ]);
    }
}
