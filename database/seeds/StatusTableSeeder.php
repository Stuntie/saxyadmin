<?php

use Illuminate\Database\Seeder;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        App\ShippingStatus::create([
            'status' => 'Ordered'
        ]);

        App\ShippingStatus::create([
            'status' => 'Picking'
        ]);

        App\ShippingStatus::create([
            'status' => 'Packing'
        ]);
        App\ShippingStatus::create([
            'status' => 'Dispatched'
        ]);

        App\ShippingStatus::create([
            'status' => 'Problem'
        ]);

        App\ShippingStatus::create([
            'status' => 'Delayed'
        ]);

    }
}
