<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Manufacturer;
use App\Type;

class ProductsController extends Controller
{
        
    /**
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('auth' );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $Manufacturers = Manufacturer::all();
        $Products = Product::orderBy('name', 'asc')->paginate(3);
        return view('products.index')->with('Products', $Products)->with('Manufacturers', $Manufacturers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
        $Manufacturers = Manufacturer::all();
        $SaxTypes = Type::all();
        return view('products.create')->with('Manufacturers', $Manufacturers)->with('SaxTypes', $SaxTypes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate input
        $this->validate($request, [
            'name' => 'required',
            'slug' => 'required',
        ]);

        // create data item
        $Product = new Product;
        $Product->name = $request->input('name');
        $Product->slug = $request->input('slug');
        $Product->type = $request->input('type');
        $Product->description = $request->input('description');
        $Product->price = $request->input('price');
        $Product->options = $request->input('options');
        $Product->image = $request->input('image');
        $Product->manufacturer_id = $request->input('manufacturer_id');
        $Product->save();

        return redirect('products')->with('Success', 'Successlly added a Product');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Product = Product::find($id);
        $Manufacturer = Manufacturer::find($Product->manufacturer_id);
        return view('products.show')->with('Product', $Product)->with('Manufacturer', $Manufacturer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Product = Product::find($id);
        $Manufacturers = Manufacturer::all();
        $SaxTypes = Type::all();
        return view('/products.edit')->with('Product', $Product)->with('Manufacturers', $Manufacturers)->with('SaxTypes', $SaxTypes);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Validate input
        $this->validate($request, [
            'name' => 'required',
            'slug' => 'required',
        ]);

        // create data item
        $Product = Product::find($id);
        $Product->name = $request->input('name');
        $Product->slug = $request->input('slug');
        $Product->type = $request->input('type');
        $Product->description = $request->input('description');
        $Product->price = $request->input('price');
        $Product->options = $request->input('options');
        $Product->image = $request->input('image');
        $Product->manufacturer_id = $request->input('manufacturer_id');
        $Product->save();

        return redirect('products')->with('Success', 'Successlly added a Product');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Product = Product::find($id);
        $Product->delete();
        return redirect('/products')->with('Success', 'Successfully removed the Product');
    }
}
