<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Manufacturer;
use App\Product;

class ManufacturersController extends Controller
{
        
    /**
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('auth' );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Manufacturers = Manufacturer::orderBy('name', 'asc')->paginate(3);
        return view('manufacturers.index')->with('Manufacturers', $Manufacturers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('manufacturers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate input
        $this->validate($request, [
            'name' => 'required',
            'slug' => 'required'
        ]);

        // create data item
        $Manufacturer = new Manufacturer;
        $Manufacturer->name = $request->input('name');
        $Manufacturer->slug = $request->input('slug');
        $Manufacturer->description = $request->input('description');
        $Manufacturer->logo = $request->input('logo');
        $Manufacturer->supplier_details = $request->input('supplier_details');
        $Manufacturer->save();

        return redirect('manufacturers')->with('Success', 'Successfully added a Manufacturer');        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Manufacturer = Manufacturer::find($id);
        $Products = Product::where('manufacturer_id', $Manufacturer->id)->get();
        return view('manufacturers.show')->with('Manufacturer', $Manufacturer)->with('Products', $Products);;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Manufacturer = Manufacturer::find($id);
               
        return view('manufacturers.edit')->with('Manufacturer', $Manufacturer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Validate input
        $this->validate($request, [
            'name' => 'required',
            'slug' => 'required'
        ]);

        // create data item
        $Manufacturer = Manufacturer::find($id);
        $Manufacturer->name = $request->input('name');
        $Manufacturer->slug = $request->input('slug');
        $Manufacturer->description = $request->input('description');
        $Manufacturer->logo = $request->input('logo');
        $Manufacturer->supplier_details = $request->input('supplier_details');
        $Manufacturer->save();

        return redirect('manufacturers')->with('Success', 'Successfully edited a Manufacturer'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Manufacturer = Manufacturer::find($id);
        $Manufacturer->delete();
        return redirect('/manufacturers')->with('Success', 'Successfully removed the Manufacturer');
    }
}
