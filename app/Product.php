<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    
    /**
     * Relationships.
     *
     * @return void
     */
    public function manufacturer(){
        return $this->belongTo('App\Manufacturer');
    }
}
