@extends('layouts.app')

@section('content')

<h1>Admins Index</h1>

<div>
    <p>
        @if(count($Admins) > 0)
            @foreach($Admins as $Admin)
                <table class="table table-striped">
                    <tr>
                        <td colspan="2"><h3>{{ $Admin->name}}</h3></td>
                        <td><a href="/admins/{{$Admin->id}}/edit" class="btn btn-secondary">Edit</a></td>
                        <td>
                            <form method="POST" action="{{ route('admins.destroy', $Admin->id ) }}" >
                                <input type="hidden" name="_method" value="Delete" >
                                @csrf
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">{{ $Admin->role}}</td>
                    </tr>
                </table>  
            @endforeach
            {{$Admins->links()}}
        @else
            <p>no Admins were found</p>
        @endif                       
        
    </p>

    <a href="/products/create" class="btn btn-secondary">Create new Product</a>
</div> 
@endsection