@extends('layouts.app')

@section('content')

<h1>Show Manufacturer Details</h1>

<div>
    <p>
        <a href="/products/create" class="btn btn-secondary">Create new Manufacturer</a>
        
        @if($Manufacturer)
                <table class="table table-striped">
                    <tr>
                        <td colspan="2"><h3>{{ $Manufacturer->name}}<br>{{ $Manufacturer->slug}}</h3></td>
                        <td><a href="/manufacturers/{{$Manufacturer->id}}/edit" class="btn btn-secondary">Edit</a></td>
                        <td>
                            <form method="POST" action="{{ route('manufacturers.destroy', $Manufacturer->id ) }}" >
                                <input type="hidden" name="_method" value="Delete" >
                                @csrf
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">{{ $Manufacturer->supplier_details}}</td>
                    </tr>
                    <tr>
                        <td colspan="4"><img src="/img/products/{{$Manufacturer->logo}}" style="max-width: 200px;"/></td>
                    </tr>

                    @if(count($Products) > 0)
                        @foreach($Products as $Product)                             
                            <tr>
                                <td colspan="4">
                                    <a href="{{ route('products.show', $Product->id ) }}" >{{$Product->name}}</a> <br>
                                    <strong>{{ $Product->type }}</strong> £{{$Product->price}}.{{$Product->pence}}  <br>
                                    {{$Product->description}} <br>

                                </td>
                            </tr>
                        @endforeach 
                    @else
                        <p>no Manufacturer details were found</p>
                    @endif
                
                </table>  
        @else
            <p>no Manufacturers details were found</p>
        @endif                       
        
    </p>
</div> 
@endsection