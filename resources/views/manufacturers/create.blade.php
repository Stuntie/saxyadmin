@extends('layouts.app')

@section('content')
    <h2>Add a new Manufacterer</h2>
    
    <form method="POST" action="{{route('manufacturers.store')}}"  >
        @csrf

        <div class="form-group">
            <label for="name">Name:</label>
            <input type="text"  name="name" class="form-control" placeholder="Name" value="" />
        </div>
        <div class="form-group">
            <label for="slug">Slug:</label>
            <input type="text"  name="slug" class="form-control" placeholder="Slug - Alt Name" value="" />
        </div>

        <div class="form-group">
            <label for="logo">logo:</label>
            <input type="text"  name="logo" class="form-control" placeholder="Logo" value="default.jpg" />
        </div>

        <div class="form-group">
            <label for="description">Description:</label>
            <input type="textarea"  name="description" class="form-control" placeholder="Description" value="" />
        </div>

        <div class="form-group">
            <label for="supplier_details">supplier_details:</label>
            <input type="textarea"  name="supplier_details" class="form-control" placeholder="Supplier Details" value="" />
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
        
        <button class="btn btn-secondary ml-4" onclick="history.go(-1)">Cancel</button>

    </form>
@endsection