@extends('layouts.app')

@section('content')

<h1>Manufacturer Index</h1>

<div>
    <p><a href="/manufacturers/create" class="btn btn-secondary">Create new Manufacturer</a></p>
    
    @if(count($Manufacturers) > 0)
        @foreach($Manufacturers as $Manufacturer)
            <table class="table table-striped">
                <tr>
                    <td colspan="2"><h3><a href="{{ route('manufacturers.show', $Manufacturer->id ) }}" >{{ $Manufacturer->name}}</a><br>{{ $Manufacturer->slug}}</h3></td>
                    <td><a href="/manufacturers/{{$Manufacturer->id}}/edit" class="btn btn-secondary">Edit</a></td>
                    <td>
                        <form method="POST" action="{{ route('manufacturers.destroy', $Manufacturer->id ) }}" >
                            <input type="hidden" name="_method" value="Delete" >
                            @csrf
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">{{ $Manufacturer->supplier_details}}</td>
                </tr>
                <tr>
                    <td colspan="4"><img src="/img/logos/{{$Manufacturer->logo}}" style="max-width: 200px;"/></td>
                </tr>
            </table>  
        @endforeach
        {{$Manufacturers->links()}}
    @else
        <p>no Manufacturers were found</p>
    @endif
</div> 
@endsection