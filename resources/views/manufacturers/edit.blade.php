@extends('layouts.app')

@section('content')
    <h2>Edit a Manufacterer</h2>
    
    <form method="POST" action="{{route('manufacturers.update', $Manufacturer->id)}}"  >
        <input type="hidden" name="_method" value="PUT" >
        @csrf

        <div class="form-group">
            <label for="name">Name:</label>
            <input type="text"  name="name" class="form-control" placeholder="Name" value="{{$Manufacturer->name}}" />
        </div>
        <div class="form-group">
            <label for="slug">Slug:</label>
            <input type="text"  name="slug" class="form-control" placeholder="Slug - Alt Name" value="{{$Manufacturer->slug}}" />
        </div>

        <div class="form-group">
            <label for="logo">logo:</label>
            <input type="text"  name="logo" class="form-control" placeholder="Logo" value="{{$Manufacturer->logo}}" />
        </div>

        <div class="form-group">
            <label for="description">Description:</label>
            <input type="textarea"  name="description" class="form-control" placeholder="Description" value="{{$Manufacturer->description}}" />
        </div>

        <div class="form-group">
            <label for="supplier_details">supplier_details:</label>
            <input type="textarea"  name="supplier_details" class="form-control" placeholder="Supplier Details" value="{{$Manufacturer->supplier_details}}" />
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
        
        <button class="btn btn-secondary ml-4" onclick="history.go(-1)">Cancel</button>

    </form>
@endsection