@extends('layouts.app')

@section('content')
    <h1>About Saxy Shop...</h1>

    <p>
    This is a Demo app to Demo a basic Laravel shop type app.<br>
    Allows Customers to view and 'buy' Saxophones of various types from Ficticious companies.<br>
    (Note: no actual eCommerce system is used in theis Demo).
    <ul>
        <li>View by Type - Alto Tenor etc.</li>
        <li>View by Saxophone Company.</li>
        <li>Register a Customer Profile.</li>
        <li>View Customer Profile details.</li>
        <li>View Customer orders.</li>
    </ul>
    </p>
@endsection
