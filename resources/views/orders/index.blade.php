@extends('layouts.app')

@section('content')

<h1>Orders Index</h1>

<div>
    <p>
        @if(count($Orders) > 0)
            @foreach($Orders as $Order)
                <table class="table table-striped">
                    <tr>
                        <td colspan="2"><h3>{{ $Order->name}}</h3></td>
                        <td><a href="/orders/{{$Order->id}}/edit" class="btn btn-secondary">Edit</a></td>
                        <td>
                            <form method="POST" action="{{ route('orders.destroy', $Order->id ) }}" >
                                <input type="hidden" name="_method" value="Delete" >
                                @csrf
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">{{ $Order->description}}</td>
                    </tr>
                </table>  
            @endforeach
            {{$Orders->links()}}
        @else
            <p>no Orders were found</p>
        @endif                       
        
    </p>

    <a href="/orders/create" class="btn btn-secondary">Create new Order</a>
</div> 
@endsection