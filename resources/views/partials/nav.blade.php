

<li class="nav-item">
    <a class="nav-link" href="/about">About </a>
</li>

<li class="nav-item">
    <a class="nav-link" href="/todo" style="color:red;">To Do List </a>
</li>

@auth
<li class="nav-item">
    <a class="nav-link" href="/products">Saxes</a>
</li>

<li class="nav-item">
    <a class="nav-link" href="/manufacturers">Manufacturers</a>
</li>

<li class="nav-item">
    <a class="nav-link" href="/orders">Orders</a>
</li>

<li class="nav-item">
    <a class="nav-link" href="/admins">Admins</a>
</li>

@endauth