@extends('layouts.app')

@section('content')
    <h2>Edit a Product</h2>
    
    <form method="POST" action="{{route('products.update', $Product->id)}}"  >
        <input type="hidden" name="_method" value="PUT" >
        @csrf

        <div class="form-group">
            <label for="name">Name:</label>
            <input type="text"  name="name" class="form-control" placeholder="Name" value="{{$Product->name}}" />
        </div>
        
        <div class="form-group">
            <label for="slug">Slug:</label>
            <input type="text"  name="slug" class="form-control" placeholder="Slug - Alt Name" value="{{$Product->slug}}" />
        </div>
        
        <div class="form-group">
            <label for="type">Type:</label>
            <select  name="type">
                @foreach ($SaxTypes as $SaxType)  
                    @if( $Product->type == $SaxType->type) 
                        <option selected value="{{$SaxType->type}}">{{$SaxType->type}}</option> 
                    @else 
                        <option value="{{$SaxType->type}}">{{$SaxType->type}}</option>
                    @endif 
                @endforeach
            </select>
        </div>
        
        <div class="form-group">
            <label for="manufacturer_id">Manufacturer:</label>
            <select  name="manufacturer_id">
                @if(count($Manufacturers) > 0)
                    @foreach ($Manufacturers as $Manufacturer)  
                        @if( $Product->manufacturer_id == $Manufacturer->id) 
                            <option selected value="{{$Manufacturer->id}}">{{$Manufacturer->name}}</option> 
                        @else 
                            <option value="{{$Manufacturer->id}}">{{$Manufacturer->name}}</option>
                        @endif 
                    @endforeach
                @endif
            </select>
        </div>

        <div class="form-group">
            <label for="Price">Price - in pounds:</label>
            <input type="number"  name="price" class="form-control" min="0" value="{{$Product->price}}" />
        </div>
        
        <div class="form-group">
            <label for="image">Image:</label>
            <input type="text"  name="image" class="form-control" placeholder="Image" value="{{$Product->image}}" />
        </div>

        <div class="form-group">
            <label for="description">Description:</label>
            <input type="textarea"  name="description" class="form-control" placeholder="Description" value="{{$Product->description}}" />
        </div>

        <div class="form-group">
            <label for="options">Options:</label>
            <input type="textarea"  name="options" class="form-control" placeholder="Options" value="{{$Product->options}}" />
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
        
        <button class="btn btn-secondary ml-4" onclick="history.go(-1)">Cancel</button>

    </form>
@endsection