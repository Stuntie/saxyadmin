@extends('layouts.app')

@section('content')

<h1>Products Index</h1>

<div>
    <p>
        <a href="/products/create" class="btn btn-secondary">Create new Product</a></p>
    <p>
        @if(count($Products) > 0)
            @foreach($Products as $Product)
                <table class="table table-striped">
                    <tr>
                        <td colspan="2"><h3><a href="{{ route('products.show', $Product->id ) }}" >{{ $Product->name}}</a><br>{{ $Product->slug}}                        </h3></td>
                        <td><a href="/products/{{$Product->id}}/edit" class="btn btn-secondary">Edit</a></td>
                        <td>
                            <form method="POST" action="{{ route('products.destroy', $Product->id ) }}" >
                                <input type="hidden" name="_method" value="Delete" >
                                @csrf
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="4">
                            Manufacturer: 
                            @if(count($Manufacturers) > 0)
                                @foreach($Manufacturers as $Manufacturer) 
                                    @if($Product->manufacturer_id == $Manufacturer->id) 
                                    {{ $Manufacturer->name}}
                                    @endif
                                @endforeach 
                            @else
                                <p>no Manufacturer details were found</p>
                            @endif 
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2"><strong>{{ $Product->type }}</strong></td>
                        <td colspan="2">&pound{{ $Product->price }}</td>
                    </tr>

                    <tr>
                        <td colspan="4">{{ $Product->description}}</td>
                    </tr>

                    <tr>
                        <td colspan="4">{{ $Product->options}}</td>
                    </tr>
                    <tr>
                        <td colspan="4"><img src="/img/products/{{$Product->image}}" style="max-width: 200px;"/></td>
                    </tr>
                </table>  
            @endforeach
            {{$Products->links()}}
        @else
            <p>no Products were found</p>
        @endif                       
        
    </p>

</div> 
@endsection