@extends('layouts.app')

@section('content')
    <h2>Create a new Product</h2>
    
    <form method="POST" action="{{route('products.store')}}"  >
        @csrf

        <div class="form-group">
            <label for="name">Name:</label>
            <input type="text"  name="name" class="form-control" placeholder="Name" value="" />
        </div>
        
        <div class="form-group">
            <label for="slug">Slug:</label>
            <input type="text"  name="slug" class="form-control" placeholder="Slug - Alt Name" value="" />
        </div>
        
        <div class="form-group">
            <label for="type">Type:</label>
            <select  name="type">
                <option >Please select a type</option>
                @foreach ($SaxTypes as $SaxType)  
                    <option value="{{$SaxType->type}}">{{$SaxType->type}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="manufacturer_id">Manufacturer:</label>
            <select  name="manufacturer_id">
                <option >Please select a Manufacturer</option>
                @if(count($Manufacturers) > 0)
                    @foreach ($Manufacturers as $Manufacturer)  
                        <option value="{{$Manufacturer->id}}">{{$Manufacturer->name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        
        <div class="form-group">
            <label for="Price">Price - in pounds:</label>
            <input type="number"  name="price" class="form-control" min="0" value="0" />
        </div>
        
        <div class="form-group">
            <label for="image">Image:</label>
            <input type="text"  name="image" class="form-control" placeholder="Image" value="default.jpg" />
        </div>

        <div class="form-group">
            <label for="description">Description:</label>
            <input type="textarea"  name="description" class="form-control" placeholder="Description" value="" />
        </div>

        <div class="form-group">
            <label for="options">Options:</label>
            <input type="textarea"  name="options" class="form-control" placeholder="Options" value="" />
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
        
        <button class="btn btn-secondary ml-4" onclick="history.go(-1)">Cancel</button>

    </form>

<hr>
<hr>

    {!! Form::open(['url' => 'tests']) !!}

    <div class="form-group">
        {{Form::label('name','Name:') }}
        {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Add product Name' ])}}
    </div>

    <div class="form-group">
        {{Form::label('slug','Slug (Alternate ID value):') }}
        {{Form::text('slug', '', ['class' => 'form-control', 'placeholder' => 'Add Slug' ])}}
    </div>
    
    <div class="form-group">
        {{Form::label('type','Type:') }}
        {{Form::text('type', '', ['class' => 'form-control', 'placeholder' => 'Add Type' ])}}
    </div>
  
    <div class="form-group">
        {{Form::label('manufacturer_id','Manufacturer:') }}
        <select  name="manufacturer_id" class="form-control">
            <option >Please select a Manufacturer</option>
            @if(count($Manufacturers) > 0)
                @foreach ($Manufacturers as $Manufacturer)  
                    <option value="{{$Manufacturer->id}}">{{$Manufacturer->name}}</option>
                @endforeach
            @endif
        </select>
    </div>
    
    
    <div class="form-group">
        {{Form::label('price','Price:') }}
        {{Form::text('Price', '', ['class' => 'form-control', 'placeholder' => 'Add Price' ])}}
    </div>
    
    <div class="form-group">
        {{Form::label('image','Image:') }}
        {{Form::text('image', '', ['class' => 'form-control', 'placeholder' => 'Add product Image' ])}}
    </div>
    
    <div class="form-group">
        {{Form::label('description','Description:') }}
        {{Form::textarea('description', '', ['class' => 'form-control', 'placeholder' => 'Add Description text' ])}}
    </div>
    
    <div class="form-group">
        {{Form::label('options','Options:') }}
        {{Form::textarea('options', '', ['class' => 'form-control', 'placeholder' => 'Add Options text' ])}}
    </div>

    {{ Form::submit('Submit', ['class' => 'btn btn-primary']) }}

    <button class="btn btn-secondary ml-4" onclick="history.go(-1)">Cancel</button>

    {!! Form::close() !!}


@endsection