Saxy Shop Admin 
===============

Demo Laravel App.

Ian Hill 2020.
This is a dummy demo 'shop app admin' I am building to help 'template' Laravel code snippets for a ficticious Saxophone shop.

To allow Admin proccesses for Products, Stock, Admin, Customers and Orders.

Migrations for DB, CRUD for data
Auth roles for Admin and Sales staff.
